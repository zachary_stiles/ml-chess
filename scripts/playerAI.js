function randomMove() {
  const legalMoves = game.moves();
  const randomIndex = Math.floor(Math.random() * legalMoves.length);
  move(legalMoves[randomIndex]);
  return legalMoves[randomIndex];
}

const ADJUST_WEIGHT = 0.5;

// TODO: move this (+ perhaps the save/load functions) to a separate JS file entirely.
let training = {}; // TODO: on page load, reload from selected file if there is one already
let gameInProgress = { w: [], b: [] };

const getPosition = () => game.fen().substring(0, game.fen().indexOf(' ') + 2);
const getTotalWeight = position => {
  const weights = Object.values(position);
  weights.pop();
  return weights.reduce((sum, weight) => (sum + weight), 0);
}

function learningAI() {  
  // TODO: actually create a learning AI lmao (WIP)
  const turn = game.turn();
  const position = getPosition();

  if (!training[position]) {
    training[position] = {};
    game.moves().forEach(move => (training[position][move] = 1));
    training[position]['total'] = Object.keys(training[position]).length;
  }

  let randomSelect = Math.random() * training[position]['total'];
  for (var theMove in training[position]) {
    randomSelect -= training[position][theMove];
    if (randomSelect <= 0) {
      move(theMove);
      gameInProgress[turn].push({ position, move: theMove });
      break;
    }
  }
}

// TEMPORARY FOR TESTING
let whiteWins = 0;
let blackWins = 0;

function processAiMoves() {
  let winner = '';
  if (game.in_checkmate()) winner = (game.turn() === 'w') ? 'b' : 'w';
  else if (countWhite !== countBlack) winner = (countWhite < countBlack) ? 'b' : 'w';

  // TEMPORARY FOR TESTING
  if (winner === 'w') whiteWins++;
  if (winner === 'b') blackWins++;
  console.log(`white ${whiteWins} --- black ${blackWins}`);

  if (winner) {
    ['w', 'b'].forEach(player => {
      gameInProgress[player].forEach(turn => {
        training[turn['position']][turn['move']] += (player === winner) ? ADJUST_WEIGHT : -ADJUST_WEIGHT;
        if (training[turn['position']][turn['move']] <= 0) {
          delete training[turn['position']][turn['move']];
          if (Object.keys(training[turn['position']]).length == 0) {
            delete training[turn['position']];
          }
        }
        training[turn['position']]['total'] = getTotalWeight(training[turn['position']]);
      });
    });
  }
};

function resetAiMoves() {
  gameInProgress = { w: [], b: [] };
}

function saveTraining() {
  const trainingText = JSON.stringify(training);
  let filename = prompt('Saving: please enter a file name.', 'training');
  if (!filename) filename = 'training';

  const lnk = document.createElement('a');
  lnk.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(trainingText));
  lnk.setAttribute('download', filename + '.txt');
  lnk.style.display = 'none';

  document.body.appendChild(lnk);
  lnk.click();
  document.body.removeChild(lnk);
}

// TODO: This isn't working for large files?
function loadTraining(e) {
  const reader = new FileReader();
  reader.onload = () => {
    training = JSON.parse(reader.result);
    // TODO: Verification and error handling.
  }
};