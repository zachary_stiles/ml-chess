let board, game;
const ai = { w: null, b: 'randomMove' };

function init() {
  game = new Chess();
  board = new ChessBoard('chessboard', { onSquareClick });

  initSettingsPanel();
  gameLoop();
}

function gameLoop() {
  if (game.game_over()) gameOver();
  else if (ai[game.turn()]) window[ai[game.turn()]]();
}

function onSquareClick(clickedSquare, selectedSquares) {
  // Can't select things while the AI is in control.
  if (ai[game.turn()]) return;

  // No square selected yet, select clicked.
  if (selectedSquares.length === 0) {
    if (game.moves({ square: clickedSquare }).length > 0) {
      board.selectSquare(clickedSquare);
    }
    return;
  }

  const selectedSquare = selectedSquares[0];
  board.unselectSquare(selectedSquare);

  // Previously selected clicked, just unselecting.
  if (clickedSquare === selectedSquare) return;

  const clickedPieceObject = game.get(clickedSquare);
  const selectedPieceObject = game.get(selectedSquare);

  // Change player selection from one piece to another.
  if (clickedPieceObject && (clickedPieceObject.color === selectedPieceObject.color)) {
    board.selectSquare(clickedSquare);
    return;
  }

  // Prevent illegal moves.
  const legalMoves = game.moves({ square: selectedSquare, verbose: true });
  if (legalMoves.filter(move => move.to === clickedSquare).length === 0) return;

  // Requested move is valid, proceed.
  if (selectedPieceObject.type === 'p' && (clickedSquare[1] === '1' || clickedSquare[1] === '8')) {
    // Offer promotion when a pawn reaches back row.
    board.askPromotion(selectedPieceObject.color, shortPiece => {
      move({ from: selectedSquare, to: clickedSquare, promotion: shortPiece });
    });
  } else {
    // For any other situation, just complete the move.
    move({ from: selectedSquare, to: clickedSquare });
  }
}

function move(selected) {
  game.move(selected);
  board.setPosition(game.fen());
  updateScores();
  setTimeout(() => gameLoop());
}

function gameOver() {
  processAiMoves();
  gameOverText();

  setTimeout(() => resetGame(), 250); // TEMPORARY FOR TESTING - continuous AI vs AI
}

function resetGame() {
  game.reset();
  board.setPosition(game.fen());

  resetAiMoves();
  resetTurnColor();
  gameLoop();
}
