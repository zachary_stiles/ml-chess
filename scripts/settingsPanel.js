function initSettingsPanel() {
  // AI select buttons.
  document.querySelectorAll('.settings__mode').forEach(button => {
    if (String(ai[button.name]) === button.value) button.checked = true;
    button.onclick = () => {
      ai[button.name] = (button.value !== 'null') ? button.value : null;
      gameLoop();
    };
  });

  // Reset game button.
  document.querySelector('.settings__reset-game').onclick = () => resetGame();
}

let countWhite = 39;
let countBlack = 39;

function updateScores() {
  const position = game.fen().substring(0, game.fen().indexOf(' '));
  const count = s => (position.match(new RegExp(s, 'g')) || []).length;

  // Count army based on standard pawn-relative valuations:
  // pawn = 1, knight / bishop = 3, rook = 5, queen = 9
  countWhite = count('P') + 3*(count('N') + count('B')) + 5*count('R') + 9*count('Q');
  countBlack = count('p') + 3*(count('n') + count('b')) + 5*count('r') + 9*count('q');
  
  // Display army values
  document.querySelector('.army-value__white').innerText = countWhite;
  document.querySelector('.army-value__black').innerText = countBlack;
  document.querySelector('.main__turn-color').innerText = (game.turn() === 'w' ? 'white' : 'black');
}

function gameOverText() {
  document.querySelector('.main__turn').style.display = 'none';
  if (game.in_checkmate()) {
    document.querySelector('.main__game-over').innerText = (game.turn() === 'w' ? 'Black' : 'White') + ' wins!';
  } else {
    document.querySelector('.main__game-over').innerText = 'It\'s a draw.';
  }
}

function resetTurnColor() {
  document.querySelector('.main__game-over').innerText = '';
  document.querySelector('.main__turn').style.display = '';
}
